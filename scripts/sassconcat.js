const fs = require('fs');
const lo = require('lodash');
const FileHound = require('filehound');
const concat = require('concat');

const path = './../common';
const out = 'allsass.txt';

const files = FileHound.create()
  .paths(path)
  .ext('scss')
  .find();

// files.then(console.log);

files.each(file => {
	const contents = fs.readFileSync(file, 'utf8');
	
	fs.appendFile(out, contents, function(err) {
	    if(err) {
	        return console.log(err);
	    }
	});
});
