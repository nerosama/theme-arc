const fs = require('fs');
const lo = require('lodash');
const getColors = require("get-svg-colors");
const FileHound = require('filehound');

const path = './../common';
const out = 'svgdeux.md';
const theme = 'flat';

const files = FileHound.create()
  .paths(path)
  .ext('svg')
  .find();

// files.then(console.log);

files.each(file => {
	const colors = getColors(file, {flat: true});
	const str = colors.toString();
	const arr = lo.uniq(str.split(','));

	if (arr[0] != '') {
		fs.appendFile(out, '\n\n' + file + '\n\n' + theme + ' => ' + arr, function(err) {
		    if(err) {
		        return console.log(err);
		    }
		});
	}

});

