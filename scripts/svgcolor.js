const fs = require('fs');
const lo = require('lodash');
const getColors = require("get-svg-colors");
const FileHound = require('filehound');

const path = './../common';
const out = 'allsvg.txt';

const files = FileHound.create()
  .paths(path)
  .ext('svg')
  .find();

// files.then(console.log);

files.each(file => {
	const colors = getColors(file, {flat: true});

	fs.appendFile(out, colors + ',', function(err) {
	    if(err) {
	        return console.log(err);
	    }
	});
});

